package com.rocketsiebel.jgifacade;

import com.rocketsiebel.jgitfacade.Diff;
import com.rocketsiebel.jgitfacade.JGitFacade;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.util.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeFalse;
import static org.junit.Assume.assumeTrue;

/**
 * @author Eugene Krivosheyev
 */
public class JGitFacadeTest {
    private static final String GIT_REPO_TEST_ROOT_DIR = "gitrepo.test";
    //region FIELDS
    private File repoDir;
    private JGitFacade jGitFacade;
    //endregion

    //region FIXTURE
    @Before
    public void setUp() throws Exception {
        repoDir = new File(GIT_REPO_TEST_ROOT_DIR);
        jGitFacade = new JGitFacade(repoDir);
    }
    //endregion

    @Test
    public void shouldGetFileNamesForAddedOrChangedOrModifiedOrUntracked() throws IOException, GitAPIException {
        assertThat(jGitFacade.getNotYetCommitedFileNames())
            .contains("addedFile", "modifiedFile", "untrackedFile", "changedFile");
    }

    @Test
    public void shouldGetDiffForAllFilesAgainstTwoDifferentCommits() throws IOException, GitAPIException {
        Diff diff = jGitFacade.getDiffForAllFilesAgainstTwoCommits("ae690", "64fb7");

        assertEquals(1, diff.getEntries().size());
        assertEquals(DiffEntry.ChangeType.MODIFY, diff.getEntries().get(0).getChangeType());
        assertEquals("fileForComparasionAgainstCommits", diff.getEntries().get(0).getOldPath());
        assertEquals("fileForComparasionAgainstCommits", diff.getEntries().get(0).getNewPath());

        assertEquals(
                "diff --git a/fileForComparasionAgainstCommits b/fileForComparasionAgainstCommits\n" +
                        "index 916b48e..d3f56f1 100644\n" +
                        "--- a/fileForComparasionAgainstCommits\n" +
                        "+++ b/fileForComparasionAgainstCommits\n" +
                        "@@ -1,3 +1,6 @@\n" +
                        "+line 1\n" +
                        "+line 2\n" +
                        " line inserted\n" +
                        " line 3\n" +
                        " line modified\n" +
                        "+line 5\n",
                diff.getText().toString());
    }

    @Test
    public void shouldNotGetDiffForAllFilesAgainstTwoSameCommits() throws IOException, GitAPIException {
        Diff diff = jGitFacade.getDiffForAllFilesAgainstTwoCommits("ae690", "ae690");

        assertEquals(0, diff.getEntries().size());
    }

    @Test
    public void shouldGetDiffForAllFilesAgainstNonStagedAndHEAD() throws GitAPIException {

        Diff diff = jGitFacade.getDiffForAllNotStagedFilesAgainstHEAD();

        assertEquals(2, diff.getEntries().size());
        assertEquals(DiffEntry.ChangeType.MODIFY, diff.getEntries().get(0).getChangeType());
        assertEquals("modifiedFile", diff.getEntries().get(0).getOldPath());
        assertEquals("modifiedFile", diff.getEntries().get(0).getNewPath());

        assertEquals(DiffEntry.ChangeType.ADD, diff.getEntries().get(1).getChangeType());
        assertEquals("/dev/null", diff.getEntries().get(1).getOldPath());
        assertEquals("untrackedFile", diff.getEntries().get(1).getNewPath());

        // Denis Tuchin: Don't understand why
        assertEquals(
                "diff --git a/modifiedFile b/modifiedFile\n" +
                        "index e69de29..d00491f 100644\n" +
                        "--- a/modifiedFile\n" +
                        "+++ b/modifiedFile\n" +
                        "@@ -0,0 +1 @@\n" +
                        "+1\n" +
                        "diff --git a/untrackedFile b/untrackedFile\n" +
                        "new file mode 100644\n" +
                        "index 0000000..a92d664\n" +
                        "--- /dev/null\n" +
                        "+++ b/untrackedFile\n" +
                        "@@ -0,0 +1,3 @@\n" +
                        "+line 1\n" +
                        "+line 2\n" +
                        "+line 3\n",
                diff.getText().toString());
    }

    @Test
    public void shouldGetFileContentsFromSpecifiedCommit() throws IOException {
        assertThat(jGitFacade.getFileContentsFromCommit("fileWithContentToFind", "20ce"))
                .contains("some\ncontent");
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionTryingGetUnexistingFileFromSpecifiedCommit() throws IOException {
        jGitFacade.getFileContentsFromCommit("unexistingFile", "20ce");
    }

    @Test
    public void shouldGetFileContent() {
        assertThat(jGitFacade.getFileContent(GIT_REPO_TEST_ROOT_DIR + "/untrackedFile"))
            .contains("line 1\nline 2\nline 3");
    }

    @Test
    public void shouldGetCommitListForParticularFileChanges() {
        List<RevCommit> revCommitList = jGitFacade.getCommitsForFileChanges("javaSourceFileToFind.java");

        assertEquals(2, revCommitList.size());
        assertThat(revCommitList.get(0).getName()).startsWith("8b959");
        assertThat(revCommitList.get(0).getShortMessage()).contains("javaSourceFileToFind.java");
        assertThat(revCommitList.get(1).getName()).startsWith("75c86");
        assertThat(revCommitList.get(1).getShortMessage()).contains("javaSourceFileToFind.java");
    }

    @Test
    public void shouldDoAddForParticularNewFile() throws IOException, GitAPIException {
        // Preparing for test
        String stagedFileName = "stagedFile";
        Status status;

        try(Git git = Git.open(repoDir)) {
            git.rm().addFilepattern(stagedFileName).call();
            prepareTestFile(git, stagedFileName);
            status = git.status().call();
        }
        assumeFalse(status.getAdded().contains(stagedFileName));
        assumeTrue(status.getUntracked().contains(stagedFileName));
        // End of preparing for test

        jGitFacade.stage(stagedFileName);

        try(Git git = Git.open(repoDir)) {
            status = git.status().call();
        }
        assertTrue(status.getAdded().contains(stagedFileName));
        assertFalse(status.getUntracked().contains(stagedFileName));
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowExceptionTryingAddForUnexistingFile() throws IOException, GitAPIException {
        String unexistingFile = "unexistingFile";
        jGitFacade.stage(unexistingFile);
    }

    @Test
    public void shouldDoAddForParticularModifiedFile() throws IOException, GitAPIException {
        // Preparing for test
        String stagedModifiesFile = "stagedModifiesFile";
        Status status;

        try(Git git = Git.open(repoDir)) {
            prepareTestFile(git, stagedModifiesFile);
            git.add().addFilepattern(stagedModifiesFile).call();
            modifyFile(git, stagedModifiesFile);
            status = git.status().call();
        }
        assumeTrue(status.getAdded().contains(stagedModifiesFile));
        assumeFalse(status.getUntracked().contains(stagedModifiesFile));
        assumeTrue(status.getModified().contains(stagedModifiesFile));
        // End of preparing for test

        jGitFacade.stage(stagedModifiesFile);

        try(Git git = Git.open(repoDir)) {
            status = git.status().call();
        }
        assertTrue(status.getAdded().contains(stagedModifiesFile));
        assertFalse(status.getUntracked().contains(stagedModifiesFile));
        assertFalse(status.getModified().contains(stagedModifiesFile));
    }

    @Test
    public void shouldDoCommitForParticularStagedFile() throws GitAPIException, IOException {
        String commitedFileName = "commitedFile";
        Status status;

        try(Git git = Git.open(repoDir)) {
            git.rm().addFilepattern(commitedFileName).call();
            git.commit().setOnly(commitedFileName).setMessage(commitedFileName + " removed").call();
            prepareTestFile(git, commitedFileName);
            git.add().addFilepattern(commitedFileName).call();
            status = git.status().call();
        }
        assumeTrue(status.getAdded().contains(commitedFileName));
        assumeFalse(status.getUntracked().contains(commitedFileName));

        jGitFacade.commit(commitedFileName, commitedFileName + " added");

        try(Git git = Git.open(repoDir)) {
            status = git.status().call();
        }
        assertFalse(status.getAdded().contains(commitedFileName));
        assertFalse(status.getUntracked().contains(commitedFileName));
    }

    @Test //TODO: Very unstable test: works for mvn, but does not for IDEA
    public void shouldGetListForCommits() throws IOException, GitAPIException {
        List<RevCommit> commits = jGitFacade.getCommits();
        assertTrue(commits.size() > 10);
        //todo: last commits
        assertThat(commits.get(0).getShortMessage()).isEqualTo("Dummy commit");
        assertThat(commits.get(1).getShortMessage()).isEqualTo("javaSourceFileToFind.java mofified");

    }

    //region PRIVATE
    private void prepareTestFile(Git git, String fileName) throws IOException {
        File file = new File(git.getRepository().getWorkTree(), fileName);
        file.delete();
        FileUtils.createNewFile(file);
        PrintWriter writer = new PrintWriter(file);
        writer.print("line 1\nline 2\nline 3");
        writer.close();
    }

    private void modifyFile(Git git, String fileName) throws IOException {
        File file = new File(git.getRepository().getWorkTree(), fileName);
        PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
        writer.print("\nline 4");
        writer.close();
    }
    //endregion
}
