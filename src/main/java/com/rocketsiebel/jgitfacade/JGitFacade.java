package com.rocketsiebel.jgitfacade;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.AbstractTreeIterator;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.gitective.core.CommitFinder;
import org.gitective.core.PathFilterUtils;
import org.gitective.core.filter.commit.CommitListFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nonnull;

/**
 * @author Eugene Krivosheyev
 *
 * Wrapper Facade for JGit API for pleasure development experience with JGit.
 * Warning! Neither subdirs nor branches supported yet.
 *
 */
public class JGitFacade {
    //region LOGGER
    private Logger log = LoggerFactory.getLogger(JGitFacade.class);
    //endregion

    //region FIELDS
    @Nonnull private File repoDir;
    //endregion

    //region CONSTRUCTORS
    public JGitFacade(@Nonnull String repoDirName) {
        this(new File(repoDirName));
    }

    public JGitFacade(@Nonnull File repoDir) {
        this.repoDir = repoDir;
    }
    //endregion

    /**
     * @return Collection of filenames at working dir in states: Added, Changed, Modified, Untracked.
     */
    public Iterable<String> getNotYetCommitedFileNames() {
        try(Git git = Git.open(repoDir)) {

            Status status = git.status().call();

            List<String> fileNames = new LinkedList<>();
            fileNames.addAll(status.getAdded());
            fileNames.addAll(status.getChanged());
            fileNames.addAll(status.getModified());
            fileNames.addAll(status.getUntracked());

            return fileNames;

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * @param commitAHash Short or long SHA string.
     * @param commitBHash Short or long SHA string.
     * @return Diff object encapsulating diff entries and plain diff text.
     */
    public Diff getDiffForAllFilesAgainstTwoCommits(String commitAHash, String commitBHash) {
        try (Git git = Git.open(repoDir)) {

            OutputStream diffText = new ByteArrayOutputStream();
            List<DiffEntry> diffEntries = git.diff().setOutputStream(diffText)
                .setOldTree(getTreeIterator(git, commitAHash))
                .setNewTree(getTreeIterator(git, commitBHash))
                .call();

            return new Diff(diffEntries, diffText.toString());

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public Diff getDiffForAllNotStagedFilesAgainstHEAD() {
        try(Git git = Git.open(repoDir)) {

            OutputStream diffText = new ByteArrayOutputStream();
            List<DiffEntry> diffEntries = git.diff().setOutputStream(diffText)
                    .call();

            return new Diff(diffEntries, diffText.toString());

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * @return File content as string with '\n' characters for new lines.
     * @throws IllegalStateException: 'Did not find expected file'
     */
    public String getFileContentsFromCommit(String fileName, String commitHash) {
        try(Git git = Git.open(repoDir)) {
            final Repository repo = git.getRepository();
            ByteArrayOutputStream fileContent = new ByteArrayOutputStream();

            ObjectId lastCommitId = repo.resolve(commitHash);
            RevWalk revWalk = new RevWalk(repo);
            RevCommit commit = revWalk.parseCommit(lastCommitId);
            RevTree tree = commit.getTree();

            TreeWalk treeWalk = new TreeWalk(repo);
            treeWalk.addTree(tree);
            treeWalk.setRecursive(true);
            treeWalk.setFilter(PathFilter.create(fileName));
            if (!treeWalk.next()) {
                throw new IllegalStateException("Did not find expected file " + fileName);
            }

            ObjectId objectId = treeWalk.getObjectId(0);
            ObjectLoader loader = repo.open(objectId);

            loader.copyTo(fileContent);
            revWalk.dispose();

            return fileContent.toString();

        } catch (IOException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * @return File content as string with '\n' characters for new lines.
     */
    public String getFileContent(String fileName) {
        try {
            String fileContent = "";
            for(String line : org.apache.commons.io.FileUtils.readLines(new File(fileName))) {
                fileContent = fileContent + line + "\n";
            }

            return fileContent;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return List of commits where particular file was changed.
     */
    public List<RevCommit> getCommitsForFileChanges(String fileName) {
        try(Git git = Git.open(repoDir)) {

            CommitFinder finder = new CommitFinder(git.getRepository());
            CommitListFilter javaCommits = new CommitListFilter();
            finder.setFilter(PathFilterUtils.andSuffix(fileName));
            finder.setMatcher(javaCommits);
            finder.find();

            return javaCommits.getCommits();

        } catch (IOException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public void stage(String fileName) {
        try(Git git = Git.open(repoDir)) {

            git.add().addFilepattern(fileName).call();

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Commit for staged file or files.
     * @param fileName Particular file name or '.' for all directory.
     * @param message Commit message. Please use git commit message convention.
     */
    public void commit(String fileName, String message) {
        try(Git git = Git.open(repoDir)) {

            git.commit()
                .setOnly(fileName)
                .setMessage(message)
                .call();

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Commit for staged and not staged file or files .
     * @param fileName Particular file name or '.' for all directory.
     * @param message Commit message. Please use git commit message convention.
     */
    public void commitAll(String fileName, String message) {
        try(Git git = Git.open(repoDir)) {

            git.commit()
                    .setOnly(fileName)
                    .setMessage(message)
                    .setAll(true)
                    .call();

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public void commitByAuthor(String fileName, String message, String authorName, String authorEmail) {
        try(Git git = Git.open(repoDir)) {

            git.commit()
                    .setOnly(fileName)
                    .setMessage(message)
                    .setAuthor(authorName, authorEmail)
                    .call();

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    public void commitByCommiter(String fileName, String message, String commiterName, String commiterEmail) {
        try(Git git = Git.open(repoDir)) {

            git.commit()
                    .setOnly(fileName)
                    .setMessage(message)
                    .setCommitter(commiterName, commiterEmail)
                    .call();

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Warning: does not work with branches yet.
     * @return List of commits.
     */
    public List<RevCommit> getCommits() {
        try(Git git = Git.open(repoDir)) {

            List<RevCommit> commits = new LinkedList<>();
            Iterable<RevCommit> commitsFromLog = git.log().call();
            for (RevCommit commit : commitsFromLog) {
                commits.add(commit);
            }

            return commits;

        } catch (IOException | GitAPIException e) {
            log.error("", e);
            throw new RuntimeException(e);
        }
    }

    //region PRIVATE
    private AbstractTreeIterator getTreeIterator(Git git, String name) throws IOException {
        final ObjectId id = git.getRepository().resolve(name);
        if (id == null) throw new IllegalArgumentException(name);

        final CanonicalTreeParser parser = new CanonicalTreeParser();
        try (ObjectReader reader = git.getRepository().newObjectReader()) {
            parser.reset(reader, new RevWalk(git.getRepository()).parseTree(id));
            return parser;
        }
    }

    //endregion
}
