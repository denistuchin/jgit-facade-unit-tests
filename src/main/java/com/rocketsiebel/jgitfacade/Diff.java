package com.rocketsiebel.jgitfacade;

import org.eclipse.jgit.diff.DiffEntry;

import java.util.List;

/**
 * @author Eugene Krivosheyev
 */
public class Diff {
    //region FIELDS
    private List<DiffEntry> entries;
    private String text;
    //endregion

    //region CONSTRUCTORS
    public Diff(List<DiffEntry> entries, String text) {
        this.entries = entries;
        this.text = text;
    }
    //endregion

    //region GETTERS
    public List<DiffEntry> getEntries() {
        return entries;
    }

    public String getText() {
        return text;
    }
    //endregion
}
